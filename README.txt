- Для демонстрации интеграции с OCR системами надо выбрать сервер с установленной Oracle JDK 8. 
- Скопировать на него(в Linux систему например в папку /home/user) jar-файл recognizer-0.0.1-SNAPSHOT.jar из архива. 
- В ту же папку скопировать tessdata из архива. 
- Далее создать переменную среды TESSDATA_PREFIX и указать на папку tessdata. Например, для Linux можно сделать это командой "export TESSDATA_PREFIX=/home/user/tessdata" 
Проверить, что переменная создана(для Linux можно проверить командой echo $TESSDATA_PREFIX) и имеет корректное значение. 
- Далее открыть командную строку и выполнить следующие команду: java -Xmx2048m -jar recognizer-0.0.1-SNAPSHOT.jar 
- Далее в браузере открыть страницу: http://localhost:8089/recognize Выбрать файл test-image для расшифровки и нажать кнопку "Загрузить". Подождать некоторое время и сохранить полученный результат 
******************************************************************************** 
Если Для Linux системы вы при запуске получите ошибку с текстом: Unable to load library 'tesseract': Native library (linux-x86-64/libtesseract.so) not found in resource path ([jar:file:/home/user/recognizer-0.0.1-SNAPSHOT.jar!/BOOT-INF/classes!/ то выполните команду "sudo apt-get install tesseract-ocr". ********************************************************************************